# -*- coding: utf-8 -*-
import json
import logging
import pprint
import urllib
import urllib2
import werkzeug
from urllib import urlencode

from odoo import http
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.http import request
from odoo.addons.payment_paytm.models import Checksum

_logger = logging.getLogger(__name__)


class PaytmController(http.Controller):
    _return_url = '/payment/paytm/ipn/'

    def paytm_validate_data(self, **post):
        res = False
        acquire = request.env['payment.acquirer'].search([('provider', '=', 'paytm')], limit=1)
        # Verify Checksum
        if Checksum.verify_checksum(post, acquire.paytm_merchant_key, post.get('CHECKSUMHASH')):
            reference = post.get('ORDERID')
            tx = None
            if reference:
                tx = request.env['payment.transaction'].search([('reference', '=', reference)])
            # Find Transaction Status API URL & Generate Checksumhash
            paytm_urls = request.env['payment.acquirer']._get_paytm_urls(tx and tx.acquirer_id.environment or 'prod')
            validate_url = paytm_urls['paytm_rest_url']
            MID = post.get('MID') or False
            ORDERID = post.get('ORDERID') or False
            CHECKSUMHASH = Checksum.generate_checksum({'MID':MID,'ORDERID':ORDERID}, acquire.paytm_merchant_key)
            
            if validate_url and MID and ORDERID:
                urequest = urllib2.Request("%s?JsonData=%s" % (validate_url, "{'MID':'"+str(MID)+"','ORDERID':'"+str(ORDERID)+"','CHECKSUMHASH':'"+CHECKSUMHASH+"'}"))
                resp = urllib2.urlopen(urequest).read()
                if isinstance(resp, str):
                    resp = eval(resp)
            
                if resp.get('STATUS') == 'TXN_SUCCESS' or resp.get('RESPCODE') == '01':
                    _logger.info('Paytm: Validated Data')
                    res = request.env['payment.transaction'].sudo().form_feedback(resp, 'paytm')
                elif resp.get('STATUS') == 'TXN_FAILURE' or resp.get('RESPCODE') == '14112':
                    _logger.warning('Paytm: Answered INVALID/FAIL on data verification')
        return res

    @http.route('/payment/paytm/ipn/', type='http', auth='none', methods=['POST'], csrf=False)
    def paytm_ipn(self, **post):
        """ Paytm Instant Payment Notification. """
        _logger.info('Beginning Paytm IPN form_feedback with post data %s', pprint.pformat(post))  # debug
        try:
            self.paytm_validate_data(**post)
        except ValidationError:
            _logger.exception('Unable to validate the Paytm payment')
        return werkzeug.utils.redirect('/shop/payment/validate')
