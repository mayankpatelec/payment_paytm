# coding: utf-8
import json
import logging
import urlparse

import dateutil.parser
import pytz

from odoo import api, fields, models, _
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.addons.payment_paytm.controllers.main import PaytmController
from odoo.tools.float_utils import float_compare
import Checksum


_logger = logging.getLogger(__name__)


class AcquirerPaytm(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('paytm', 'Paytm')])
    paytm_website = fields.Char('Paytm Website', help='Website provided by Paytm', default='DIYtestingweb')
    paytm_merchant_id = fields.Char(
        'Paytm Merchant ID', groups='base.group_user',
        help='The Merchant ID is used to ensure communications coming from Paytm are valid and secured.', default='DIY12386817555501617')
    paytm_merchant_key = fields.Char(
        'Paytm Merchant Key', groups='base.group_user', default='bKMfNxPPf_QdZppa')
    
    def _get_feature_support(self):
        """Get advanced feature support by provider.

        Each provider should add its technical in the corresponding
        key for the following features:
            * fees: support payment fees computations
            * authorize: support authorizing payment (separates
                         authorization and capture)
            * tokenize: support saving payment data in a payment.tokenize
                        object
        """
        res = super(AcquirerPaytm, self)._get_feature_support()
        #res['fees'].append('paytm')
        return res

    @api.model
    def _get_paytm_urls(self, environment):
        """ Paytm URLS """
        if environment == 'prod':
            return {
                'paytm_form_url': 'https://secure.paytm.in/oltp-web/processTransaction?orderid=%3COrder_ID',
                'paytm_rest_url': 'https://secure.paytm.in/oltp/HANDLER_INTERNAL/TXNSTATUS',
            }
        else:
            return {
                'paytm_form_url': 'https://pguat.paytm.com/oltp-web/processTransaction?orderid=%3COrder_ID',
                'paytm_rest_url': 'https://pguat.paytm.com/oltp/HANDLER_INTERNAL/TXNSTATUS',
            }

    @api.multi
    def paytm_compute_fees(self, amount, currency_id, country_id):
        """ Compute paytm fees.

            :param float amount: the amount to pay
            :param integer country_id: an ID of a res.country, or None. This is
                                       the customer's country, to be compared to
                                       the acquirer company country.
            :return float fees: computed fees
        """
        if not self.fees_active:
            return 0.0
        country = self.env['res.country'].browse(country_id)
        if country and self.company_id.country_id.id == country.id:
            percentage = self.fees_dom_var
            fixed = self.fees_dom_fixed
        else:
            percentage = self.fees_int_var
            fixed = self.fees_int_fixed
        fees = (percentage / 100.0 * amount + fixed) / (1 - percentage / 100.0)
        return fees

    @api.multi
    def paytm_form_generate_values(self, values):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        paytm_tx_values = {}
        paytm_tx_values.update({
            'REQUEST_TYPE': 'DEFAULT',
            'MID': self.paytm_merchant_id,
            'ORDER_ID': values['reference'],
            'TXN_AMOUNT': values['amount'],
            'CHANNEL_ID': 'WEB',
            'INDUSTRY_TYPE_ID': 'Retail',
            'WEBSITE': self.paytm_website,
            'CUST_ID': values.get('partner_name') or partner.name,
            
            'MOBILE_NO': values.get('partner_phone'),
            'EMAIL': values.get('partner_email'),
            'ADDRESS_1': values.get('partner_address'),
            'CITY': values.get('partner_city'),
            'STATE': values.get('partner_state') and (values.get('partner_state').code or values.get('partner_state').name) or '',
            'PINCODE': values.get('partner_zip'),
            'CALLBACK_URL': '%s' % urlparse.urljoin(base_url, PaytmController._return_url),
        })
        CHECKSUMHASH = Checksum.generate_checksum(paytm_tx_values, self.paytm_merchant_key)
        paytm_tx_values.update({
            'CHECKSUMHASH': CHECKSUMHASH,
        })
        return paytm_tx_values

    @api.multi
    def paytm_get_form_action_url(self):
        return self._get_paytm_urls(self.environment)['paytm_form_url']


class TxPaytm(models.Model):
    _inherit = 'payment.transaction'

    paytm_txn_type = fields.Char('Transaction type')

    # --------------------------------------------------
    # FORM RELATED METHODS
    # --------------------------------------------------

    @api.model
    def _paytm_form_get_tx_from_data(self, data):
        reference, txn_id = data.get('ORDERID'), data.get('TXNID')
        if not reference or not txn_id:
            error_msg = _('Paytm: received data with missing reference (%s) or txn_id (%s)') % (reference, txn_id)
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        # find tx -> @TDENOTE use txn_id ?
        txs = self.env['payment.transaction'].search([('reference', '=', reference)])
        if not txs or len(txs) > 1:
            error_msg = 'Paytm: received data for reference %s' % (reference)
            if not txs:
                error_msg += '; no order found'
            else:
                error_msg += '; multiple order found'
            _logger.info(error_msg)
            raise ValidationError(error_msg)
        return txs[0]

    @api.multi
    def _paytm_form_get_invalid_parameters(self, data):
        invalid_parameters = []
        return invalid_parameters

    @api.multi
    def _paytm_form_validate(self, data):
        status = data.get('STATUS')
        res = {
            'acquirer_reference': data.get('TXNID'),
            'paytm_txn_type': data.get('PAYMENTMODE'),
        }
        if status in ['TXN_SUCCESS']:
            _logger.info('Validated Paytm payment for tx %s: set as done' % (self.reference))
            try:
                # dateutil and pytz don't recognize abbreviations PDT/PST
                tzinfos = {
                    'PST': -8 * 3600,
                    'PDT': -7 * 3600,
                }
                date_validate = dateutil.parser.parse(data.get('TXNDATE'), tzinfos=tzinfos).astimezone(pytz.utc)
            except:
                date_validate = fields.Datetime.now()
            res.update(state='done', date_validate=date_validate)
            return self.write(res)
        elif status in ['TXN_PENDING']:
            _logger.info('Received notification for Paytm payment %s: set as pending' % (self.reference))
            res.update(state='pending', state_message=data.get('RESPMSG', ''))
            return self.write(res)
        else:
            error = 'Received unrecognized status for Paytm payment %s: %s, set as error' % (self.reference, status)
            _logger.info(error)
            res.update(state='error', state_message=error)
            return self.write(res)
