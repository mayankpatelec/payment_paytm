# -*- coding: utf-8 -*-

{
    'name': 'Payment Acquirer Paytm',
    'summary': 'Payment Acquirer: Paytm Implementation',
    'version': '10.0.1',
    'author': 'Mayank Patel',
    'category': 'Accounting',
    'depends': [
        'payment',
    ],
    'demo': [],
    'data': [
        'views/payment_paytm_templates.xml',
        'data/payment_acquirer_data.xml',
        'views/payment_views.xml',
    ],
    'installable': True,
}
